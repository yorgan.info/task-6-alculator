﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Сalculator
{
    internal class Mathematics
    {

        public void Add(double oper1, double oper2, out double sum)
        {
            sum = oper1 + oper2;
        }

        public void Sub(double oper1, double oper2, out double sum)
        {
            sum = oper1 - oper2;
        }

        public void Mul(double oper1, double oper2, out double sum)
        {
            sum = oper1 * oper2;
        }

        public void Div(double oper1, double oper2, out double sum)
        {
            sum = oper1 / oper2;
        }

        public void OutRes(double oper1, double oper2, string operand, double sum)
        {
            Console.WriteLine(new String('=',50));
            Console.WriteLine($"Результат операції {oper1} {operand} {oper2} = {sum}");
            Console.WriteLine(new String('=', 50));
        }

    }
}
