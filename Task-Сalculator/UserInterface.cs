﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Сalculator
{
    internal class UserInterface
    {

        public double InNumData(string info = "Введіть число: ")
        {

            double operand;
            while (true)
            {
                try
                {
                    Console.Write(info);
                    operand = double.Parse(Console.ReadLine());
                    break;
                }
                catch
                {
                    ShowError();
                }
            }
            return operand;
        }

        public string InMathOperator(string info = "Введіть оператор: ")
        {

            string mathOperator;
            while (true)
            {
                Console.Write(info);
                mathOperator = Console.ReadLine().Replace(" ", "");

                if (mathOperator == "+" || mathOperator == "-" || mathOperator == "*" || mathOperator == "/")
                {
                    break;
                }
                else
                {
                    ShowError();
                }
            }

            return mathOperator;
        }

        public void ShowError(string error = "Помилка")
        {

            ConsoleColor color = Console.ForegroundColor;

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(error);
            Console.ForegroundColor = color;

        }

    }
}
