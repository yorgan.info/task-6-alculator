﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_Сalculator
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Mathematics mathematics = new Mathematics();
            UserInterface userInterface = new UserInterface();


            double sum = default;
            while (true)
            {
                double operand1 = userInterface.InNumData("Введить число 1: ");
                double operand2 = userInterface.InNumData("Введить число 2: ");

                string mathOperator = userInterface.InMathOperator();


                switch (mathOperator)
                {
                    case "+":
                        {
                            mathematics.Add(operand1, operand2, out sum);
                            mathematics.OutRes(operand1, operand2, mathOperator, sum);
                            break;
                        }
                    case "-":
                        {
                            mathematics.Sub(operand1, operand2, out sum);
                            mathematics.OutRes(operand1, operand2, mathOperator, sum);
                            break;
                        }
                    case "*":
                        {
                            mathematics.Mul(operand1, operand2, out sum);
                            mathematics.OutRes(operand1, operand2, mathOperator, sum);
                            break;
                        }
                    case "/":
                        {
                            if (operand1 != 0 && operand2 != 0)
                            {
                                mathematics.Div(operand1, operand2, out sum);
                                mathematics.OutRes(operand1, operand2, mathOperator, sum);
                            }
                            else
                            {
                                userInterface.ShowError("На нуль діліти не можна!");
                            }
                            break;
                        }
                    default:
                        {
                            userInterface.ShowError("Дивно все повинно працювати(");
                            break;
                        }

                        
                }

                Console.Write("\nНатисни на будь яку кнопку щоб продовжити!");
                Console.ReadKey();
                Console.Clear();
            }

        }
    }
}
